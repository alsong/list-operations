#include <stdio.h>
#include <stdlib.h>

struct Node {
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

// Function implementations
struct Node *createNode(int num) {
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (!newNode) {
        printf("Error: Memory allocation failed.\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

void printList(struct Node *head) {
    struct Node *current = head;
    if (current == NULL) {
        printf("List is empty.\n");
        return;
    }
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;
        if (current != NULL) printf(", ");
    }
    printf("]\n");
}

void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void deleteByKey(struct Node **head, int key) {
  if (*head == NULL) {
    return;
  }

  if (key == 0) {
    // Handle deletion of the head node
    struct Node *temp = *head;
    *head = temp->next;
    free(temp);
    return;
  }

  struct Node *temp = *head;
  for (int i = 0; temp != NULL && i < key - 1; i++) {
    temp = temp->next;
  }

  if (temp == NULL || temp->next == NULL) {
    return;
  }

  struct Node *nextToNext = temp->next->next;
  free(temp->next);
  temp->next = nextToNext;
}

void deleteByValue(struct Node **head, int value) {
    // Implementation for deleting the first node with the specified value
    struct Node *temp = *head, *prev = NULL;

    if (temp != NULL && temp->number == value) {
        *head = temp->next;
        free(temp);
        return;
    }

    while (temp != NULL && temp->number != value) {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL) return;

    prev->next = temp->next;
    free(temp);
}

void insertAfterKey(struct Node **head, int key, int value) {
    if (*head == NULL) return;

    struct Node *temp = *head;
    struct Node *newNode = createNode(value);

    // If the key is 0, insert the new node after the head
    if (key == 0) {
        newNode->next = temp->next;
        temp->next = newNode;
        return;
    }

    // Traverse the list to find the node at the specified key
    for (int i = 0; temp != NULL && i < key - 1; i++) {
        temp = temp->next;
    }

    // If the key is beyond the end of the list, do not insert
    if (temp == NULL || temp->next == NULL) return;

    // Insert the new node after the found node
    newNode->next = temp->next;
    temp->next = newNode;
}

void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    // Implementation for inserting a node after a node containing a specific value
     struct Node *temp = *head;

    while (temp != NULL && temp->number != searchValue) {
        temp = temp->next;
    }

    if (temp == NULL) return;

    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}

// Main function
int main() {
    struct Node *head = NULL;
    int choice, data, key;

    while (1) {
        printf("\nLinked Lists Operations\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter the key to delete: ");
                scanf("%d", &key);
                deleteByKey(&head, key);
                break;
            case 5:
                printf("Enter the value to delete: ");
                scanf("%d", &data);
                deleteByValue(&head, data);
                break;
            case 6:
                printf("Enter the key and value to insert: ");
                scanf("%d %d", &key, &data);
                insertAfterKey(&head, key, data);
                break;
            case 7:
                printf("Enter the search value and new value to insert: ");
                scanf("%d %d", &data, &key);
                insertAfterValue(&head, data, key);
                break;
            case 8:
                printf("Exiting...\n");
                return 0;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
