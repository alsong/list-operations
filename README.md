# Linked List Implementation in C

This C program demonstrates a simple implementation of a singly linked list with basic operations such as append, prepend, delete, and insert. Below is an explanation of the key components and functionalities of the program.

## Structure Definition
struct Node {

int number;

struct Node *next; 

};


- **Node Structure**: Each node in the list contains an integer (`number`) and a pointer to the next node (`next`).

## Core Functions

- **`createNode(int num)`**: Allocates memory for a new node with the specified number.
- **`printList(struct Node *head)`**: Displays the contents of the list starting from the head node.
- **`append(struct Node **head, int num)`**: Adds a new node with the given number to the end of the list.
- **`prepend(struct Node **head, int num)`**: Inserts a new node with the given number at the beginning of the list.
- **`deleteByKey(struct Node **head, int key)`**: Removes the node at the specified position (key) in the list.
- **`deleteByValue(struct Node **head, int value)`**: Deletes the first node found with the specified value.
- **`insertAfterKey(struct Node **head, int key, int value)`**: Inserts a new node with the given value after a node at a specified position (key).
- **`insertAfterValue(struct Node **head, int searchValue, int newValue)`**: Adds a new node with the given value immediately after the first node with the specified search value.

## Program Flow

The `main` function presents a menu-driven interface to the user, allowing them to perform various operations on the linked list:

1. **Print the List**: Outputs the current contents of the list.
2. **Append a Node**: Adds a node to the end of the list.
3. **Prepend a Node**: Inserts a node at the start of the list.
4. **Delete a Node by Key**: Removes a node based on its position in the list.
5. **Delete a Node by Value**: Deletes the first node with a specific value.
6. **Insert After a Key**: Adds a node after a specified position.
7. **Insert After a Value**: Inserts a node following the first occurrence of a specified value.
8. **Exit**: Terminates the program.

## Usage

Users interact with the program through a console interface, selecting operations by entering the corresponding number. For actions that require additional data (e.g., the number to be added or the key for deletion), the program prompts the user for input.

This linked list implementation showcases fundamental operations for managing singly linked lists in C, including node creation, insertion, deletion, and traversal.

